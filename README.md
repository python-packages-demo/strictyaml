# strictyaml

StrictYAML is a type-safe YAML parser that parses and validates a restricted subset of the YAML specification. [strictyaml](https://pypi.org/project/strictyaml)